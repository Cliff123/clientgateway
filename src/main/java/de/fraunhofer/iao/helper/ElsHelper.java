package de.fraunhofer.iao.helper;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import de.fraunhofer.iao.clientgateway.model.server.sensordata.SensorData;

public class ElsHelper {
    public static String sortedSearchBody(String sortDirection) {
        return "{\"query\":{\"match_all\":{}},\"sort\":{\"time\":\"" + sortDirection + "\"}}";
    }
}
