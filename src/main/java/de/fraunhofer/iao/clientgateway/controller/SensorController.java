package de.fraunhofer.iao.clientgateway.controller;

import com.google.common.base.Stopwatch;
import com.google.gson.*;
import de.fraunhofer.iao.clientgateway.config.Config;
import de.fraunhofer.iao.clientgateway.model.client.sensordata.AxisValueData;
import de.fraunhofer.iao.clientgateway.model.client.sensordata.ClientAxisValue;
import de.fraunhofer.iao.clientgateway.model.client.sensordata.SingleValue;
import de.fraunhofer.iao.clientgateway.model.client.sensordata.SingleValueData;
import de.fraunhofer.iao.clientgateway.model.server.sensordata.helper.AxisValue;
import de.fraunhofer.iao.helper.ElsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class SensorController {

    private static final Logger log = LoggerFactory.getLogger(SensorController.class);

    // localhost:8080/v1/devices/xdk110/sensors/singlevalue/lightSensorData
    @GetMapping(value = "/v1/devices/{deviceId}/sensors/singlevalue/{sensor}")
    @ResponseBody
    public SingleValueData readSingleValueData(@PathVariable String deviceId, @PathVariable String sensor) {

        Stopwatch stopwatch = Stopwatch.createStarted();

        RestTemplate restTemplate = new RestTemplate();

        String body = ElsHelper.sortedSearchBody("asc");
        // "http://elasticsearch-elk.193.197.89.142.nip.io/test_17_10/xdk110/_search?pretty=true&q=*:*";
        String url = "http://"
                        + Config.getElasticUrlOrDefault()
                        + ":"
                        + Config.getElasticPortOrDefault()
                        + "/"
                        + Config.getElasticIndexOrDefault()
                        + "/"
                        + deviceId
                        + "/_search";

        String rawJson = restTemplate.postForObject(url, body, String.class);

        JsonObject json = new JsonParser().parse(rawJson).getAsJsonObject();
        JsonArray hits = json.get("hits").getAsJsonObject().get("hits").getAsJsonArray();
        SingleValueData svd = new SingleValueData();
        List<SingleValue> values = new ArrayList<>();
        for (JsonElement element : hits) {
            JsonObject source = element.getAsJsonObject().get("_source").getAsJsonObject();
            JsonObject sensorData = source.get(sensor).getAsJsonObject();

            String unit = sensorData.get("unit").getAsString();
            svd.setUnit(unit);

            SingleValue value = new SingleValue();
            value.setTime(source.get("time").getAsLong());
            value.setValue(sensorData.get("value").getAsInt());
            values.add(value);
        }
        svd.setValues(values);

        log.info("Responding " + svd.getValues().size() + " records of " + sensor + ". Took " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " ms");
        return svd;
    }

    // localhost:8080/v1/devices/xdk110/sensors/axisvalue/magnetometerData
    @GetMapping(value = "/v1/devices/{deviceId}/sensors/axisvalue/{sensor}")
    @ResponseBody
    public AxisValueData readAxisValueData(@PathVariable String deviceId, @PathVariable String sensor) {

        Stopwatch stopwatch = Stopwatch.createStarted();

        RestTemplate restTemplate = new RestTemplate();

        String body = ElsHelper.sortedSearchBody("asc");
        // "http://elasticsearch-elk.193.197.89.142.nip.io/test_17_10/xdk110/_search?pretty=true&q=*:*";
        String url = "http://"
                + Config.getElasticUrlOrDefault()
                + ":"
                + Config.getElasticPortOrDefault()
                + "/"
                + Config.getElasticIndexOrDefault()
                + "/"
                + deviceId
                + "/_search?pretty=true&q=*:*";

        String rawJson = restTemplate.postForObject(url, body, String.class);

        JsonObject json = new JsonParser().parse(rawJson).getAsJsonObject();
        JsonArray hits = json.get("hits").getAsJsonObject().get("hits").getAsJsonArray();
        AxisValueData avd = new AxisValueData();
        List<ClientAxisValue> clientAxisValues = new ArrayList<>();
        for (JsonElement element : hits) {
            JsonObject source = element.getAsJsonObject().get("_source").getAsJsonObject();
            JsonObject sensorData = source.get(sensor).getAsJsonObject();

            String unit = sensorData.get("unit").getAsString();
            avd.setUnit(unit);

            ClientAxisValue clientAxisValue = new ClientAxisValue();
            clientAxisValue.setTime(source.get("time").getAsLong());

            AxisValue axisValue = new AxisValue();
            JsonObject jsonValues = sensorData.get("values").getAsJsonObject();
            axisValue.setX(jsonValues.get("x").getAsInt());
            axisValue.setY(jsonValues.get("y").getAsInt());
            axisValue.setZ(jsonValues.get("z").getAsInt());
            clientAxisValue.setValues(axisValue);
            clientAxisValues.add(clientAxisValue);
        }
        avd.setValues(clientAxisValues);

        log.info("Responding " + avd.getValues().size() + " records of " + sensor + ". Took " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " ms");
        return avd;
    }
}
