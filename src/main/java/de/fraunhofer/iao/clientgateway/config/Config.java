package de.fraunhofer.iao.clientgateway.config;

public class Config {

    // Environment variable names
    private static final String ENV_ELASTIC_URL = "ELASTIC_URL";
    private static final String ENV_ELASTIC_PORT = "ELASTIC_PORT";
    private static final String ENV_ELASTIC_INDEX = "ELASTIC_INDEX";

    // Default Values
    private static final String DEF_ELASTIC_URL = "elasticsearch";
    private static final String DEF_ELASTIC_PORT = "9200";
    private static final String DEF_ELASTIC_INDEX = "test_17_10";

    public static String getElasticUrlOrDefault() {
        String envUrl = System.getenv(ENV_ELASTIC_URL);
        if (envUrl != null && !envUrl.isEmpty()) {
            return envUrl;
        } else {
            return DEF_ELASTIC_URL;
        }
    }

    public static String getElasticPortOrDefault() {
        String envPort = System.getenv(ENV_ELASTIC_PORT);
        if (envPort != null && !envPort.isEmpty()) {
            return envPort;
        } else {
            return DEF_ELASTIC_PORT;
        }
    }

    public static String getElasticIndexOrDefault() {
        String envUrl = System.getenv(ENV_ELASTIC_INDEX);
        if (envUrl != null && !envUrl.isEmpty()) {
            return envUrl;
        } else {
            return DEF_ELASTIC_INDEX;
        }
    }
}
