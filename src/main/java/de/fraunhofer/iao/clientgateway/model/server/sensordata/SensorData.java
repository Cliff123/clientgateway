package de.fraunhofer.iao.clientgateway.model.server.sensordata;

import de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors.*;

public class SensorData {

    public static final String locationPrefix = "location:";

    // Common data
    private Long time;
    private String location;

    private LightSensorData lightSensorData;
    private PressureData pressureData;
    private TemperatureData temperatureData;
    private HumidityData humidityData;
    private MagnetometerData magnetometerData;
    private GyroscopeData gyroscopeData;
    private AccelerometerData accelerometerData;

    public void setTime(Long time) {
        this.time = time;
    }

    public static String getLocationPrefix() {
        return locationPrefix;
    }

    public Long getTime() {
        return time;
    }

    public String getLocation() {
        return location;
    }

    public LightSensorData getLightSensorData() {
        return lightSensorData;
    }

    public PressureData getPressureData() {
        return pressureData;
    }

    public TemperatureData getTemperatureData() {
        return temperatureData;
    }

    public HumidityData getHumidityData() {
        return humidityData;
    }

    public MagnetometerData getMagnetometerData() {
        return magnetometerData;
    }

    public GyroscopeData getGyroscopeData() {
        return gyroscopeData;
    }

    public AccelerometerData getAccelerometerData() {
        return accelerometerData;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setLightSensorData(LightSensorData lightSensorData) {
        this.lightSensorData = lightSensorData;
    }

    public void setPressureData(PressureData pressureData) {
        this.pressureData = pressureData;
    }

    public void setTemperatureData(TemperatureData temperatureData) {
        this.temperatureData = temperatureData;
    }

    public void setHumidityData(HumidityData humidityData) {
        this.humidityData = humidityData;
    }

    public void setMagnetometerData(MagnetometerData magnetometerData) {
        this.magnetometerData = magnetometerData;
    }

    public void setGyroscopeData(GyroscopeData gyroscopeData) {
        this.gyroscopeData = gyroscopeData;
    }

    public void setAccelerometerData(AccelerometerData accelerometerData) {
        this.accelerometerData = accelerometerData;
    }

    @Override
    public String toString() {
        return "SensorData{" +
                "time=" + time +
                ", location='" + location + '\'' +
                ", lightSensorData=" + lightSensorData +
                ", pressureData=" + pressureData +
                ", temperatureData=" + temperatureData +
                ", humidityData=" + humidityData +
                ", magnetometerData=" + magnetometerData +
                ", gyroscopeData=" + gyroscopeData +
                ", accelerometerData=" + accelerometerData +
                '}';
    }
}