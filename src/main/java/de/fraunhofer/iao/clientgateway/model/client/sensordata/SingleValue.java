package de.fraunhofer.iao.clientgateway.model.client.sensordata;

public class SingleValue {

    private Long time;
    private Integer value;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
