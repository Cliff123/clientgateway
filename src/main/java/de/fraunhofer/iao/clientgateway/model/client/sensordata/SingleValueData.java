package de.fraunhofer.iao.clientgateway.model.client.sensordata;

import java.util.List;

public class SingleValueData {

    private String unit;
    private List<SingleValue> values;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<SingleValue> getValues() {
        return values;
    }

    public void setValues(List<SingleValue> values) {
        this.values = values;
    }
}
