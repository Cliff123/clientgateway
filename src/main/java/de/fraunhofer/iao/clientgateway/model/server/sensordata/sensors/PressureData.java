package de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors;

public class PressureData {

    private int value;
    private String unit;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "PressureData{" +
                "value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }
}
