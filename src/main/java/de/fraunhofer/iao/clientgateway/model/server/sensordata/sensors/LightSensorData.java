package de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors;

public class LightSensorData {

    private int value;
    private String unit;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "LightSensorData{" +
                "value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }
}
