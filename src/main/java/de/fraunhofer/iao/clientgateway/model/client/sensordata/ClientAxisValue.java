package de.fraunhofer.iao.clientgateway.model.client.sensordata;

import de.fraunhofer.iao.clientgateway.model.server.sensordata.helper.AxisValue;

public class ClientAxisValue {

    private Long time;
    private AxisValue values;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public AxisValue getValues() {
        return values;
    }

    public void setValues(AxisValue values) {
        this.values = values;
    }
}
