package de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors;

public class HumidityData {

    private int value;
    private String unit;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "HumidityData{" +
                "value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }
}
