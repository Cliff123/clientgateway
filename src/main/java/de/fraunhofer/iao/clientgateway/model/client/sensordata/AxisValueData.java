package de.fraunhofer.iao.clientgateway.model.client.sensordata;

import java.util.List;

public class AxisValueData {

    private String unit;
    private List<ClientAxisValue> values;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<ClientAxisValue> getValues() {
        return values;
    }

    public void setValues(List<ClientAxisValue> values) {
        this.values = values;
    }
}
