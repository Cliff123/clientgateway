package de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors;

public class TemperatureData {

    private int value;
    private String unit;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TemperatureData{" +
                "value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }
}
