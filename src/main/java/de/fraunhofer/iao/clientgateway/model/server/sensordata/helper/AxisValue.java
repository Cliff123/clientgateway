package de.fraunhofer.iao.clientgateway.model.server.sensordata.helper;

public class AxisValue {

    private int x;
    private int y;
    private int z;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "AxisValue{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
