package de.fraunhofer.iao.clientgateway.model.server.sensordata.sensors;

import de.fraunhofer.iao.clientgateway.model.server.sensordata.helper.AxisValue;

public class MagnetometerData {

    private AxisValue values;
    private String unit;

    public AxisValue getValues() {
        return values;
    }

    public void setValues(AxisValue values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "MagnetometerData{" +
                "values=" + values +
                ", unit='" + unit + '\'' +
                '}';
    }
}
