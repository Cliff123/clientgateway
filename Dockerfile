FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD target/clientgateway-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8081

ENTRYPOINT exec java -jar /app.jar
